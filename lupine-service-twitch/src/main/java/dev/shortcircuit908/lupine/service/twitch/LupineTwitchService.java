package dev.shortcircuit908.lupine.service.twitch;

import dev.shortcircuit908.lupine.plugin.LupinePlugin;
import dev.shortcircuit908.lupine.plugin.PluginManager;

public class LupineTwitchService extends LupinePlugin {
	private final Config config = new Config();
	private LupineTwitchClient client;
	
	protected LupineTwitchService(PluginManager plugin_manager) {
		super(plugin_manager);
	}
	
	@Override
	public String getName() {
		return "TwitchService";
	}
	
	@Override
	public String getVersion() {
		return "1.0-SNAPSHOT";
	}
	
	@Override
	public void onLoad() {
		client = new LupineTwitchClient(this);
	}
	
	@Override
	public void onEnable() {
	
	}
	
	@Override
	public void onDisable() {
	
	}
	
	public LupineTwitchClient getTwitchClient(){
		return client;
	}
	
	public Config getTwitchConfig(){
		return config;
	}
	
	public class Config {
		public String getBotName() {
			return getConfig().getString("bot_name");
		}
		
		public String getBotOAuth() {
			return getConfig().getString("bot_oauth");
		}
		
		public String getChannel() {
			return getConfig().getString("channel_name");
		}
		
		public long getVoteDurationMillis() {
			return getConfig().getLong("vote.duration") * 1000L;
		}
		
		public long getVoteCooldownMillis() {
			return getConfig().getLong("vote.cooldown") * 1000L;
		}
	}
}
