package dev.shortcircuit908.lupine.service.twitch.listener;

import com.github.twitch4j.chat.events.AbstractChannelEvent;
import com.github.twitch4j.chat.events.channel.ExtendSubscriptionEvent;
import com.github.twitch4j.chat.events.channel.GiftSubscriptionsEvent;
import com.github.twitch4j.chat.events.channel.PrimeSubUpgradeEvent;
import com.github.twitch4j.chat.events.channel.SubscriptionEvent;
import com.github.twitch4j.common.enums.SubscriptionPlan;

public class LupineSubEvent {
	private final AbstractChannelEvent event;
	private final SubscriptionPlan subscription_plan;
	private final int num_subscriptions;
	
	public LupineSubEvent(GiftSubscriptionsEvent event) {
		this(event, SubscriptionPlan.fromString(event.getSubscriptionPlan()), event.getCount());
	}
	
	public LupineSubEvent(ExtendSubscriptionEvent event) {
		this(event, event.getSubPlan());
	}
	
	public LupineSubEvent(SubscriptionEvent event) {
		this(event, event.getSubPlan());
	}
	
	public LupineSubEvent(PrimeSubUpgradeEvent event) {
		this(event, event.getSubscriptionPlan());
	}
	
	private LupineSubEvent(AbstractChannelEvent event, SubscriptionPlan subscription_plan) {
		this(event, subscription_plan, 1);
	}
	
	private LupineSubEvent(AbstractChannelEvent event, SubscriptionPlan subscription_plan, int num_subscriptions) {
		this.event = event;
		this.subscription_plan = subscription_plan;
		this.num_subscriptions = num_subscriptions;
	}
	
	public AbstractChannelEvent getEvent(){
		return event;
	}
	
	public SubscriptionPlan getSubscriptionPlan(){
		return subscription_plan;
	}
	
	public int getNumSubscriptions(){
		return num_subscriptions;
	}
}
