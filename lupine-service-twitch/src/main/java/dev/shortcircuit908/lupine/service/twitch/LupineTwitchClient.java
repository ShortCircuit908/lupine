package dev.shortcircuit908.lupine.service.twitch;

import com.github.philippheuer.credentialmanager.domain.OAuth2Credential;
import com.github.twitch4j.TwitchClient;
import com.github.twitch4j.TwitchClientBuilder;
import com.github.twitch4j.chat.events.channel.ExtendSubscriptionEvent;
import com.github.twitch4j.chat.events.channel.GiftSubscriptionsEvent;
import com.github.twitch4j.chat.events.channel.PrimeSubUpgradeEvent;
import com.github.twitch4j.chat.events.channel.SubscriptionEvent;
import dev.shortcircuit908.lupine.service.twitch.listener.LupineSubEvent;
import dev.shortcircuit908.lupine.service.twitch.listener.SubListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;

public class LupineTwitchClient implements Closeable {
	private final LupineTwitchService plugin;
	private final Logger logger = LoggerFactory.getLogger(LupineTwitchClient.class);
	private final TwitchClient client;
	
	public LupineTwitchClient(LupineTwitchService plugin) {
		this.plugin = plugin;
		OAuth2Credential credential = new OAuth2Credential(
				plugin.getTwitchConfig().getBotName(),
				plugin.getTwitchConfig().getBotOAuth()
		);
		this.client = TwitchClientBuilder.builder()
				.withChatAccount(credential)
				.withDefaultAuthToken(credential)
				.withEnablePubSub(true)
				.withEnableChat(true)
				.build();
		logger.info("Twitch client connected");
		joinChannel();
	}
	
	public TwitchClient getClient() {
		return client;
	}
	
	public void registerSubEventListener(final SubListener sub_listener) {
		client.getEventManager()
				.onEvent(GiftSubscriptionsEvent.class, event -> sub_listener.accept(new LupineSubEvent(event)));
		client.getEventManager()
				.onEvent(ExtendSubscriptionEvent.class, event -> sub_listener.accept(new LupineSubEvent(event)));
		client.getEventManager()
				.onEvent(SubscriptionEvent.class, event -> sub_listener.accept(new LupineSubEvent(event)));
		client.getEventManager()
				.onEvent(PrimeSubUpgradeEvent.class, event -> sub_listener.accept(new LupineSubEvent(event)));
	}
	
	private void joinChannel() {
		logger.info("Joining channel " + plugin.getTwitchConfig().getChannel());
		client.getChat().joinChannel(plugin.getTwitchConfig().getChannel());
	}
	
	public void sendChatMessage(String message) {
		logger.info("Sending message: " + message);
		client.getChat().sendMessage(plugin.getTwitchConfig().getChannel(), message);
	}
	
	@Override
	public void close() {
		logger.info("Twitch client closing");
		client.close();
	}
}
