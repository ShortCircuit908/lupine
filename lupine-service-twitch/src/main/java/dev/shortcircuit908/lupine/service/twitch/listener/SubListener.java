package dev.shortcircuit908.lupine.service.twitch.listener;

import dev.shortcircuit908.lupine.effect.EffectDispatcher;

import java.util.function.Consumer;

public class SubListener implements Consumer<LupineSubEvent> {
	private final EffectDispatcher effect_dispatcher;
	
	public SubListener(EffectDispatcher effect_dispatcher) {
		this.effect_dispatcher = effect_dispatcher;
	}
	
	@Override
	public void accept(LupineSubEvent event) {
		for (int i = 0; i < event.getNumSubscriptions(); i++) {
			effect_dispatcher.dispatchRandomEffect();
		}
	}
}
