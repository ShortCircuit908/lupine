package dev.shortcircuit908.lupine.service.twitch.listener;

import com.github.twitch4j.chat.events.channel.CheerEvent;
import dev.shortcircuit908.lupine.service.twitch.LupineTwitchClient;

import java.util.function.Consumer;

public class CheerListener implements Consumer<CheerEvent> {
	private final LupineTwitchClient client;
	
	public CheerListener(LupineTwitchClient client) {
		this.client = client;
	}
	
	@Override
	public void accept(CheerEvent event) {
	
	}
}
