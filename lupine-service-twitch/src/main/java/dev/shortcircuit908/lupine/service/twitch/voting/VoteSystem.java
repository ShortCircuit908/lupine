package dev.shortcircuit908.lupine.service.twitch.voting;

import com.github.twitch4j.chat.events.channel.ChannelMessageEvent;
import dev.shortcircuit908.lupine.service.twitch.LupineTwitchClient;
import dev.shortcircuit908.lupine.service.twitch.LupineTwitchService;
import dev.shortcircuit908.lupine.service.twitch.listener.ChatListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class VoteSystem {
    private final LupineTwitchService plugin;
    private static final Logger logger = LoggerFactory.getLogger(VoteSystem.class);
    private Timer system_timer;
    private final Random random = new Random();
    private String last_effect = null;
    private boolean alternate_index = true;
    private String[] vote_choices;
    private Map<String, Integer> ballot_box = new HashMap<>();
    private int index_offset;

    public VoteSystem(LupineTwitchService plugin) {
        this.plugin = plugin;
        this.
        registerEventListeners();
    }

    public String[] getVoteChoices() {
        return vote_choices;
    }

    public Map<Integer, Integer> getIndexedVoteResults() {
        Map<Integer, Integer> results = new HashMap<>();
        for (int i = 0; i < vote_choices.length; i++) {
            results.put(i, 1);
        }
        for (int index : ballot_box.values()) {
            results.compute(index, (key, old_value) -> old_value == null ? 1 : old_value + 1);
        }
        return results;
    }

    public int getIndexOffset() {
        return index_offset;
    }

    public void castVote(String user_id, int indexed_vote) {
        if (indexed_vote < 0 || indexed_vote >= vote_choices.length) {
            return;
        }
        logger.info(String.format("%1$s voted for index %2$s", user_id, indexed_vote));
        ballot_box.put(user_id, indexed_vote);
    }

    public void registerEventListeners() {
        plugin.getTwitchClient().getClient()
                .getEventManager()
                .onEvent(ChannelMessageEvent.class, new ChatListener(this));
    }

    public void announceGame(String game) {
        logger.info(String.format("Announcing game to Twitch chat: %1$s", game));
        plugin.getTwitchClient().sendChatMessage(String.format("The streamer is now playing %1$s", game));
    }

    public void announceVote() {
        StringJoiner joiner = new StringJoiner(" | ");
        for (int i = 0; i < vote_choices.length; i++) {
            joiner.add(String.format("%1$s:%2$s", (i + index_offset) + 1, vote_choices[i]));
        }
        logger.info(String.format("Announcing vote to Twitch chat: %1$s", joiner));
        plugin.getTwitchClient().sendChatMessage("Voting has begun! Type the number in chat to cast your vote!");
        plugin.getTwitchClient().sendChatMessage(joiner.toString());
    }

    public void startVote(String[] vote_choices, int index_offset) {
        logger.info("New vote: " + Arrays.toString(vote_choices));
        this.vote_choices = vote_choices;
        this.index_offset = index_offset;
        ballot_box.clear();
    }

    private void startVoting() {
        if (system_timer != null) {
            system_timer.cancel();
            system_timer.purge();
        }
        system_timer = new Timer("EffectDispatcher");
        system_timer.scheduleAtFixedRate(
                new TaskStartVote(),
                0,
                plugin.getTwitchConfig().getVoteDurationMillis() + plugin.getTwitchConfig().getVoteCooldownMillis()
        );
    }

    private class TaskStartVote extends TimerTask {
        @Override
        public void run() {
            logger.info("Starting vote");
            alternate_index = !alternate_index;
            initAvailableEffects();
            Map<String, Effect> local_effects = new HashMap<>(available_effects);
            if (last_effect != null) {
                local_effects.remove(last_effect);
            }
            int num_choices = Math.min(plugin.getTwitchConfig().getChoicesPerVote(), local_effects.size());
            List<Map.Entry<String, Effect>> effect_list = new ArrayList<>(local_effects.entrySet());
            Collections.shuffle(effect_list, random);
            String[] choices = effect_list.stream().limit(num_choices).map(Map.Entry::getKey).toArray(String[]::new);
            startVote(choices, alternate_index ? plugin.getTwitchConfig().getChoicesPerVote() : 0);
            announceVote();
            system_timer.schedule(new TaskGetVoteResults(), plugin.getTwitchConfig().getVoteDurationMillis());
        }
    }

    private class TaskGetVoteResults extends TimerTask {
        @Override
        public void run() {
            logger.info("Collecting vote results");
            Map<Integer, Integer> results = vote_system.getIndexedVoteResults();
            WeightedRandomBag<Integer> bag = new WeightedRandomBag<>();
            results.forEach(bag::addEntry);
            int selected_index = bag.selectRandom(random);
            String effect = getVoteChoices()[selected_index];
            logger.info(effect + " won the vote");
            last_effect = effect;
            Effect to_dispatch = available_effects.get(effect);
            logger.info("Dispatching effect: " + effect);
            dispatchEffect(to_dispatch);
        }
    }
}
