package dev.shortcircuit908.lupine.service.twitch.voting;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WeightedRandomBag<T> {
	private List<Entry<T>> entries = new ArrayList<>();
	private double accumulated_weight;
	
	public WeightedRandomBag(){
	
	}
	
	public void addEntry(T object, double weight) {
		accumulated_weight += weight;
		Entry<T> entry = new Entry<>();
		entry.value = object;
		entry.accumulated_weight = accumulated_weight;
		entries.add(entry);
	}
	
	public T selectRandom(Random random) {
		double rand_val = random.nextDouble() * accumulated_weight;
		
		for (Entry<T> entry : entries) {
			if (entry.accumulated_weight >= rand_val) {
				return entry.value;
			}
		}
		return null;
	}
	
	private static class Entry<T> {
		double accumulated_weight;
		T value;
	}
}
