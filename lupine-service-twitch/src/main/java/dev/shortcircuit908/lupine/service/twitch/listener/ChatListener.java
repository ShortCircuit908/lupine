package dev.shortcircuit908.lupine.service.twitch.listener;

import com.github.twitch4j.chat.events.channel.ChannelMessageEvent;
import dev.shortcircuit908.lupine.service.twitch.voting.VoteSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class ChatListener implements Consumer<ChannelMessageEvent> {
    private static final Logger logger = LoggerFactory.getLogger(ChatListener.class);
    private final VoteSystem vote_system;

    public ChatListener(VoteSystem vote_system) {
        this.vote_system = vote_system;
    }

    @Override
    public void accept(ChannelMessageEvent event) {
        String message = event.getMessage();
        logger.debug("{}: {}", event.getUser().getName(), message);
        try {
            int index = Integer.parseInt(message);
            vote_system.castVote(event.getUser().getId(), (index - vote_system.getIndexOffset()) - 1);
        }
        catch (NumberFormatException e) {
            // Do nothing
        }
    }
}
