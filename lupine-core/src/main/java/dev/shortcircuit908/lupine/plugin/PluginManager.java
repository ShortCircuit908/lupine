package dev.shortcircuit908.lupine.plugin;

import java.nio.file.Path;

public class PluginManager {
	private final Path plugin_directory;
	
	public PluginManager(Path plugin_directory) {
		this.plugin_directory = plugin_directory;
	}
	
	public Path getPluginDirectory() {
		return plugin_directory;
	}
}
