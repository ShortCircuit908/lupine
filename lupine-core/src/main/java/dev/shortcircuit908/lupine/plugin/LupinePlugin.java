package dev.shortcircuit908.lupine.plugin;

import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

public abstract class LupinePlugin {
	private final YAMLConfiguration configuration = new YAMLConfiguration();
	private final PluginManager plugin_manager;
	private final Path data_directory;
	private final Logger logger;
	
	protected LupinePlugin(PluginManager plugin_manager) {
		this.logger = LoggerFactory.getLogger(getName());
		this.plugin_manager = plugin_manager;
		this.data_directory = plugin_manager.getPluginDirectory().resolve(getName());
	}
	
	public abstract String getName();
	
	public abstract String getVersion();
	
	public abstract void onLoad();
	
	public abstract void onEnable();
	
	public abstract void onDisable();
	
	public final YAMLConfiguration getConfig() {
		return configuration;
	}
	
	private File getConfigFile() {
		return getDataDirectory().resolve("config.yml").toFile();
	}
	
	public final void loadConfig() {
		configuration.clear();
		
		File config_file = getConfigFile();
		if (!config_file.exists()) {
			return;
		}
		
		try (FileReader reader = new FileReader(config_file)) {
			configuration.read(reader);
		}
		catch (FileNotFoundException e) {
			logger.warn("Configuration file not found");
		}
		catch (IOException e) {
			logger.warn("Unable to open configuration file");
			e.printStackTrace();
		}
		catch (ConfigurationException e) {
			logger.warn("Unable to parse configuration file");
			e.printStackTrace();
		}
	}
	
	public final void saveConfig() {
		File config_file = getConfigFile();
		try (FileWriter writer = new FileWriter(config_file)) {
			configuration.write(writer);
			writer.flush();
		}
		catch (ConfigurationException | IOException e) {
			logger.warn("Unable to write configuration file");
			e.printStackTrace();
		}
	}
	
	public final void saveDefaultConfig() {
		saveDefaultConfig(false);
	}
	
	public final void saveDefaultConfig(boolean overwrite) {
		File config_file = getConfigFile();
		if (config_file.exists() && !overwrite) {
			return;
		}
		try (InputStream in = getClass().getResourceAsStream("/config.yml");
			 OutputStream out = new FileOutputStream(config_file)) {
			if (in == null) {
				throw new FileNotFoundException("Resource /config.yml not found");
			}
			byte[] buffer = new byte[2048];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			out.flush();
		}
		catch (IOException e) {
			logger.warn("Unable to save default configuration file");
			e.printStackTrace();
		}
	}
	
	public final Path getDataDirectory() {
		return data_directory;
	}
	
	public final PluginManager getPluginManager() {
		return plugin_manager;
	}
}
