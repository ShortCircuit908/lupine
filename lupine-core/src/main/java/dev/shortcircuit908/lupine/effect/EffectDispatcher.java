package dev.shortcircuit908.lupine.effect;

public abstract class EffectDispatcher {
	public abstract void dispatchEffect(Effect effect);
	
	public abstract void clearEffect(Effect effect);
	
	public abstract void clearEffects();
}
